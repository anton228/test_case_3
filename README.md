# Test Task
### Python version is 3.10
## For start project:
* create virtual environment
* export pythonpath:
    * ```export PYTHONPATH=$(pwd)```
* install requirements:  
    * ```pip install -r requirements.txt```
* run web app:    
    * ```python manage.py runserver 8000 --settings=test_case_3.settings```
* run pyqt app:
    * ```cd visual```
    * ```python run.py```
* enjoy
